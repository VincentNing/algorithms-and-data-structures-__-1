using System;
using System.Collections;
using System.Text;
using System.Globalization;

namespace assessment_1_for_algorithms_and_data_structures
{
    class Palindrome //individual class for question 2
    {
        public void Checking()//created a methord
        {
            Console.WriteLine("Type in a string that I can check for you if it is a palindrome");//statement
            var y = Console.ReadLine();//read the user's input
            y = y.Replace(" ", String.Empty);// removing all spaces
            y = y.ToUpper();//Ignore capitalization
            var removingPunctuation = new StringBuilder();//using system.text to remove punctuation

            foreach (char c in y)//using foreach loop to cheack if it is a punctuation
            {
                if (!char.IsPunctuation(c))
                    removingPunctuation.Append(c);//if it is, remove it!
            }

            y = removingPunctuation.ToString();//output string after removing punctuation
            // Console.WriteLine(y);//Using for Debug
            
            Stack stack = new Stack();//created a stack to get the last value of the character
            Queue queue = new Queue();//created a queue to get the first value of the character
            int x = 0;//variable for chancing the set of value which can be found at line 41
            
            try//if there is no error
            {

                var array = y.ToCharArray();//thansfer the user input to indivadual charcters
                
                for (int i = 0; i < y.Length; i++)// a for loop of pushing the array into the stack and the queue
                {
             
                    Char temp = array[i];
                  
                    stack.Push(temp);
                    queue.Enqueue(temp);

                }

                while (queue.Count != 0)//loop for checking if the array is totally uploaded into the stack and the queue
                {
                    // Console.WriteLine(stack.Peek())     //function:Debug;
                    // Console.WriteLine(queue.Peek())     //function:Debug;
                    if (queue.Dequeue().ToString() != stack.Pop().ToString())// the brain of the whole code,using the adventage of queue which has FIFO,and the adventage of stack which has FILO to check if it is a palindrome
                    {
                        Console.WriteLine("Not a palindrome");//statement if it is not
                        break;//result comes out, break the look
                    }
                    else
                    {

                        ++x;//otherwise, keeping checking next set of value
                        continue;//keep doing the loop
                    }
                }

                if (x == y.Length)//if the time of using the loop reaches the length of user input,show the result
                {
                    Console.WriteLine("It is a palindrome!"); //statement 
                }
            }
            catch (InvalidCastException e)//if there is an error
            {
                Console.WriteLine("No characters in the stack" + e.Message);
            }
        }
        
    }
}
