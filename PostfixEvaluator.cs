using System;
using System.Collections.Generic;


namespace assessment_1_for_algorithms_and_data_structures
{
    // public class Intermediate
    // {
    //     public string expr;     // subexpression string
    //     public string oper;     // the operator used to create this expression

    //     public Intermediate(string expr, string oper)
    //     {
    //         this.expr = expr;
    //         this.oper = oper;
    //     }
    // }
    class PostfixEvaluator
    {
        static Stack<int> stack = new Stack<int>();//created the stack for storage
        public List<string> postfixArray = new List<string>();//using list for sperate the user input
        string st;//variable for the user input
        public int result;//varible using for result
        public void PostfixToInfix()//main method created

        {
            Console.WriteLine("Type in a postfix code, then it will be transfered to infix code!");//statement
            st = Console.ReadLine();//read the userinput
            for (int k = 0; k < st.Length; k++)//using for loop to sperate the string type user input to array 
            {
                postfixArray.Add(st[k].ToString());
            }
            postfixArray.Add(")");//using for loop to sperate the string type user input to array 
            for (var i=0;postfixArray[i] != ")";i++)//When the right parenthesis character is encountered, no further processing is necessary. 
            {
                //b)	When the right-parenthesis character has not been encountered
                if (" ".Contains(postfixArray[i]))//if it is a empty space, ignore it!
                {

                }
                else if (!("()*/+-^%".Contains(postfixArray[i])))//If the current character is a digit, Push its integer value on the stack 
                {

                    int temp;
                    Int32.TryParse(postfixArray[i], out temp);//convert string to intager
                    stack.Push(temp);
                    
                }
                else if ("*/+-^%".Contains(postfixArray[i]))//Otherwise, if the current character is an operator: Pop the two top elements of the stack into variables x and y. 
                {
                    var x = stack.Pop();//variable x created for taking value from stack
                    var y = stack.Pop();//variable y created for taking value from stack
                   // Console.WriteLine(y+""+ postfixArray[i]+""+ x);//using for texting
                    var calculation = Calculate(y, postfixArray[i], x);//using method Calculate to calculate 2 variables
                    
                    stack.Push(calculation);//Push the result of the calculation onto the stack. 

                }
  
            }
            Console.WriteLine(stack.Pop());//c)	When the right parenthesis is encountered in the expression, pop the top value of the stack. This is the result of the postfix expression. 
//End

        }
        public int Calculate(int op1, string operators, int op2)//method Calculate created,takes y as op1, x as op2, operators is the operator
        {
            //using switch to inditfiy 6 different operators and implment the equaltion get the result!
            switch (operators) 
            {
                case "+":
                    result = op1 + op2;
                    break;
                case "-":
                    result = op1 - op2;
                    break;
                case "*":
                    result = op1 * op2;
                    break;
                case "/":
                    result = op1 / op2;
                    break;
                case "^":
                    result = op1 ^ op2;
                    break;
                case "%":
                    result = op1 % op2;
                    break;

            }
            return result;//reture the value after implment the equaltion!

        }

    }


}
