using System;
using System.Collections.Generic;


namespace assessment_1_for_algorithms_and_data_structures
{
    class InfixToPostfixConverter
    {
        static Stack<string> stack = new Stack<string>();//created the stack for storage
        public Queue<string> postfix = new Queue<string>();//using a queue for output
        public List<string> infixArray = new List<string>();//using list for sperate the user input
        string st;//variable for the user input
        public int i = 0;//varible that is needed
        public string result;//result

        public void ConvertToPostfix()//main class created
        {
            Console.WriteLine("Type in a mathmatic equaltion, then it will be transfered to Postfix code!");//statement
            st = Console.ReadLine();//read the userinput

            for (int k = 0; k < st.Length; k++)
            {
                infixArray.Add(st[k].ToString());
            }//using for loop to sperate the string type user input to array 

            stack.Push("(");//statem a)R = Console.ReadLine();//reading user's input

            infixArray.Add(")");//statement b)Append a right parenthesis ')' to the end of infix. 

            while (i < infixArray.Count)//statement c)While the stack is not empty, read infix from left to right and do the following: 
            {
                if (!("()*/+-^%".Contains(infixArray[i])))
                {
                    postfix.Enqueue(infixArray[i]);
                }//If the current character in infix is a digit, append it to postfix. 
                else if (infixArray[i].Equals("("))
                {
                    stack.Push("(");
                }//If the current character in infix is a left parenthesis, push it onto the stack. 
                else if (IsOperator().Contains(infixArray[i]))
                {
                    Precedence();
                    stack.Push(infixArray[i]);//Push the current character in infix onto the stack. 
                }//If the current character in infix is an operator: 
                else if (infixArray[i].Equals(")"))//If the current character in infix is a right parenthesis: 
                {
                    while (IsOperator().Contains(stack.Peek()) && !(stack.Peek().Equals("(")))//Pop operators from the top of the stack.
                    {
                        postfix.Enqueue(stack.Pop());//append them to postfix until a left parenthesis is at the top of the stack. 
                    }

                    stack.Pop();//Pop (and discard) the left parenthesis from the stack. d) 
                }

                i++;//
            }
            foreach (var item in postfix)//using foreach loop to get the string type output
            {
                result += item;
            }
            Console.WriteLine(result);
        }


        public string IsOperator()//class IsOperator created, for me individually, this is easier, so I just set a string in it.
        {
            
            return "*+-%^/";
        }
        public void Precedence()//class Precedence created
        {
            if (IsOperator().Contains(stack.Peek()))//Pop operators (if there are any) at the top of the stack while they have equal or higher precedence than the current operator, and append the popped operators to postfix. 
            {
                if (infixArray[i].Equals("+") || infixArray[i].Equals("-"))//lowest precedence first
                {
                    while (IsOperator().Contains(stack.Peek()))//checking if there are anyoperators
                    {
                        postfix.Enqueue(stack.Pop());//append the popped operators to postfix. 
                    }
                }
                else if (infixArray[i].Equals("*") || infixArray[i].Equals("/"))//higher then last one
                {
                    while (("*%^/").Contains(stack.Peek()))//checking if there are anyoperators has higher precedence
                    {
                        postfix.Enqueue(stack.Pop());//append the popped operators to postfix. 
                    }
                }
                else if (infixArray[i].Equals("^") || infixArray[i].Equals("%"))//higher then last one
                {
                    while (("%^").Contains(stack.Peek()))//checking if there are anyoperators has higher precedence
                    {
                        postfix.Enqueue(stack.Pop());//append the popped operators to postfix. 
                    }
                }
            }
        }
    }
}




