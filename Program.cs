﻿using System;

namespace assessment_1_for_algorithms_and_data_structures
{
    class Program
    {
        static public string YorN;
        static void Main(string[] args)
        {
            
            do
            {
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$              Welcome to the puzzle of Vincent              $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$   Choose a number below to implement different programs!   $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$              1.Writing Queue Implementation                $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                      2.Palindromes                         $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$              3.Evaluating Expressions with a Stack         $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$       4.Evaluating a Postfix Expression with a Stack       $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                var read = Console.ReadLine();
                if (read == "1")
                {
                    Q1.q1();
                    Console.WriteLine("Do you want to repeat the program again, 'y' for yes, 'n' for no.");
                    YorN = Console.ReadLine();
        
                }
                else if (read == "2")
                {
                    Q2.q2();
                    Console.WriteLine("Do you want to repeat the program again, 'y' for yes, 'n' for no.");
                    YorN = Console.ReadLine();
                }
                else if (read == "3")
                {
                    Q3.q3();
                    Console.WriteLine("\nDo you want to repeat the program again, 'y' for yes, 'n' for no.");
                    YorN = Console.ReadLine();
                }
                else if (read == "4")
                {
                    Q4.q4();
                    Console.WriteLine("\nDo you want to repeat the program again, 'y' for yes, 'n' for no.");
                    YorN = Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Reading error, please insert again!");
                    YorN ="y";
                }

            }while(YorN=="y");
            if(YorN =="n")
            {               
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                Thank you for using our program             $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$               Have a lovly time with your life             $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                We home to see u next time here             $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                    We were happy having you                $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                          GoodBye!!!!                       $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$                                                            $$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            }
            else
            {
                Console.WriteLine("Error,reboot the peogram please!");
            }
        }

    }
    class Q1
    {
        static public void q1()
        {
            MyQueue qq = new MyQueue();
            qq.Enqueue();
            qq.Dequeue();
            qq.printQueue();
            qq.Contains();
            qq.ToArray();
        }
            
    }


    class Q2
    {
        static public void q2()
        {
            Palindrome p = new Palindrome();
            p.Checking();
        }

    }
    class Q3
    {
        static public void q3()
        {
            InfixToPostfixConverter x = new InfixToPostfixConverter();
            x.ConvertToPostfix();
        }

    }
    class Q4
    {
        static public void q4()
        {
            PostfixEvaluator x = new PostfixEvaluator();
            x.PostfixToInfix();
        }
    }

}
