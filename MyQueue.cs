using System;
using System.Collections;

namespace assessment_1_for_algorithms_and_data_structures
{
    class MyQueue//class created
    {
        public Queue q = new Queue() { };//Create a queue
        public string R = ""; // This is the variable for later recognizing 'yes' or 'no'!
        public void printQueue()//first methord that reqired
        {
            Console.WriteLine("Current queue: ");//statment
            foreach (var c in q) Console.Write(c + " ");//using foreach loop to get items in the q individually
        }
        public void Enqueue()//second methord that required
        {
            do
            {

                Console.WriteLine("please enter a item to be stored in the queue");//statment
                q.Enqueue(Console.ReadLine());//read user input

                Console.WriteLine("DO u want to add another one? 'y' for yes,'n' for no");//asking user's opinion
                R = Console.ReadLine();//read user input
            } while (R == "y");//loop for if they want to do it again

            if (R != "y" && R != "n")
            {
                Console.WriteLine("Error!");
            }//what happens when users do not follow instraction
            else
            {
                Console.WriteLine("The queue that u typed is: ");//statment
                foreach (var c in q) Console.Write(c + " ");//using foreach loop to get items in the q individually
            }//what it looks like the queue after adding

        }
        public void Dequeue()//third methord reqired
        {
            do
            {
                Console.WriteLine("\nDo u want to remove a item from queue?  'y' for yes,'n' for no");//statment and asking for opinion
                R = Console.ReadLine();//read the user's input

                if (R == "y")//recognize the user's opinion
                {
                    var ch = (string)q.Dequeue();//deleting a item from queue
                    Console.WriteLine("The removed value: {0}", ch);//message what just been done
                }

            } while (R == "y");//loop for if they want to do it again

            if (R != "y" && R != "n")
            {
                Console.WriteLine("Error!");
            }//what happens when users do not follow instraction

        }
        public void Contains()//forth methord that required
        {

            do
            {
                Console.WriteLine("\nEnter a item, I will check for u if it is a item in the queue!");//asking user's input
                var a = Console.ReadLine(); //reading the input and store it with 'a' 

                if (q.Contains(a))//checking if it is contained in the queue
                {
                    Console.WriteLine("Yes!! It is a part of the queue!!");//when it is posstive
                    Console.WriteLine("Do u want to check another one? 'y' for yes,'n' for no!");//asking for a loop?
                }
                else
                {
                    Console.WriteLine("Sorry, It is not a part of the queue");//when it is nagetive
                    Console.WriteLine("Do u want to check another one? 'y' for yes,'n' for no!");//asking for a loop?
                }
                R = Console.ReadLine();//reading user's input

            } while (R == "y" );//loop for if they want to do it again

            if (R != "y" && R != "n")
            {
                Console.WriteLine("Error!");
            }//what happens when users do not follow instraction

        }
        public void ToArray()//fifth methord required
        {
            Console.ReadLine();
            var x = q.ToArray();//transfer to an array
            Console.WriteLine("Transfer to an array : ");//what has just been done
            for (int i = 0; i < x.Length; i++)//using for loop to print the array
            {
                Console.Write(x[i] + " ");
                
            }
            Console.ReadLine();
        }

    }
}
